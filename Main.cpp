#include <iostream>
#include <fstream>
#include <string>
#include "Eleve.h"
#include "Salle.h"
#include "Table.h"
#include "Promotion.h"
#include "MainWindow.h"
#include <QtWidgets/QApplication>
#include <QApplication>
#include <QCheckBox>
#include <QtGui>
#include <qobject.h>
#include <qlabel.h>
#include <qgraphicsscene.h>
#include <qgraphicsview.h>
#include<qgridlayout.h>
#include <qgraphicsproxywidget.h>
#include <qtextcodec.h>
#include <qfiledialog.h>

using namespace std;



void affichage_salle(MainWindow& w, Salle* s, Promotion* p, QGraphicsScene& scene) {
    

   /* QGraphicsProxyWidget* proxy1 = new QGraphicsProxyWidget();
    QPushButton* ajouter_eleve = new QPushButton("Ajouter eleve");
    ajouter_eleve->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    ajouter_eleve->setFixedSize(170, 40);
    proxy1->setWidget(ajouter_eleve);
    proxy1->setPos(1100, 500);
    scene.addItem(proxy1);*/

    QGraphicsProxyWidget* proxy2 = new QGraphicsProxyWidget();
    QPushButton* valider = new QPushButton("Valider");
    valider->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    valider->setFixedSize(170, 40);
    proxy2->setWidget(valider);
    proxy2->setPos(1750, 1000);
    scene.addItem(proxy2);

    QGraphicsProxyWidget* proxy3 = new QGraphicsProxyWidget();
    QPushButton* supprimer_table = new QPushButton("Supprimer table");
    supprimer_table->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    supprimer_table->setFixedSize(170, 40);
    proxy3->setWidget(supprimer_table);
    proxy3->setPos(1300, 500);
    scene.addItem(proxy3);
    QObject::connect(supprimer_table, SIGNAL(clicked()), &w, SLOT(supprimerTable()));

    QGraphicsProxyWidget* proxy4 = new QGraphicsProxyWidget();
    QPushButton* echanger = new QPushButton("Echanger");
    echanger->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    echanger->setFixedSize(170, 40);
    proxy4->setWidget(echanger);
    proxy4->setPos(1500, 500);
    QObject::connect(echanger, SIGNAL(clicked()), &w, SLOT(echange()));
    scene.addItem(proxy4);

    QGraphicsProxyWidget* proxy5 = new QGraphicsProxyWidget();
    QPushButton* ajouter_table = new QPushButton("Ajouter table");
    ajouter_table->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    ajouter_table->setFixedSize(170, 40);
    proxy5->setWidget(ajouter_table);
    proxy5->setPos(1100, 550);
    scene.addItem(proxy5);
    QObject::connect(ajouter_table, SIGNAL(clicked()), &w, SLOT(ajout_table_supprimer()));

    QGraphicsProxyWidget* proxy6 = new QGraphicsProxyWidget();
    QPushButton* melanger = new QPushButton("Melanger");
    melanger->setStyleSheet("QPushButton { background-color: #4E5BCD }");

    melanger->setFixedSize(170, 40);
    proxy6->setWidget(melanger);
    proxy6->setPos(1500, 550);
    QObject::connect(melanger, SIGNAL(clicked()), &w, SLOT(melanger_eleve()));
    scene.addItem(proxy6);

    /*QGraphicsProxyWidget* proxy7 = new QGraphicsProxyWidget();
    QPushButton* retirer_eleve = new QPushButton("Retirer eleve");
    retirer_eleve->setStyleSheet("QPushButton { background-color: #4E5BCD }");

    retirer_eleve->setFixedSize(170, 40);
    proxy7->setWidget(retirer_eleve);
    proxy7->setPos(1300, 550);
    scene.addItem(proxy7);*/

    QGraphicsProxyWidget* proxy8 = new QGraphicsProxyWidget();
    QPushButton* quitter = new QPushButton("Quitter");
    quitter->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    quitter->setFixedSize(170, 40);
    QObject::connect(quitter, SIGNAL(clicked()),qApp, SLOT(quit()));
    proxy8->setWidget(quitter);
    proxy8->setPos(1750, 0);
    scene.addItem(proxy8);

    QGraphicsProxyWidget* proxy9 = new QGraphicsProxyWidget();
    QPushButton* prenom = new QPushButton("Prenom");
    prenom->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    prenom->setFixedSize(80, 40);
    QObject::connect(prenom, SIGNAL(clicked()), &w, SLOT(classement_prenom()));
    proxy9->setWidget(prenom);
    proxy9->setPos(280, 0);
    scene.addItem(proxy9);


    QGraphicsProxyWidget* proxy10 = new QGraphicsProxyWidget();
    QPushButton* nom = new QPushButton("Nom");
    nom->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    nom->setFixedSize(80, 40);
    QObject::connect(nom, SIGNAL(clicked()), &w, SLOT(classement_nom()));
    proxy10->setWidget(nom);
    proxy10->setPos(280, 40);
    scene.addItem(proxy10);


    QGraphicsProxyWidget* proxy11 = new QGraphicsProxyWidget();
    QPushButton* table = new QPushButton("Table");
    table->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    table->setFixedSize(80, 40);
    QObject::connect(table, SIGNAL(clicked()), &w, SLOT(classement_place()));
    proxy11->setWidget(table);
    proxy11->setPos(280, 80);
    scene.addItem(proxy11);

    QGraphicsProxyWidget* proxy_checkbox_grp = new QGraphicsProxyWidget();
    QGraphicsProxyWidget* proxy_checkbox_demi = new QGraphicsProxyWidget();
    QGraphicsProxyWidget* proxy_checkbox_tiers = new QGraphicsProxyWidget();

    QCheckBox* checkbox_grp = new QCheckBox(" Groupe");
    checkbox_grp->setFixedSize(170, 40);
    checkbox_grp->setStyleSheet("QCheckBox:indicator { width: 50px;height: 50px;spacing: 30px;}");
    proxy_checkbox_grp->setWidget(checkbox_grp);
    proxy_checkbox_grp->setPos(900, 600);
    scene.addItem(proxy_checkbox_grp);

    QCheckBox* checkbox_demi = new QCheckBox("Demi-groupe");
    checkbox_demi->setFixedSize(170, 40);
    checkbox_demi->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    proxy_checkbox_demi->setWidget(checkbox_demi);
    proxy_checkbox_demi->setPos(900, 550);
    scene.addItem(proxy_checkbox_demi);
 

    QCheckBox* checkbox_tiers = new QCheckBox("Tiers-temps");
    checkbox_tiers->setFixedSize(170, 40);
    checkbox_tiers->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    proxy_checkbox_tiers->setWidget(checkbox_tiers);
    proxy_checkbox_tiers->setPos(900, 500);
    scene.addItem(proxy_checkbox_tiers);
   
  QGraphicsProxyWidget* proxyliste = new QGraphicsProxyWidget();
    QPushButton* liste = new QPushButton("Export list");
    liste->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    liste->setFixedSize(170, 40);
    proxyliste->setWidget(liste);
    proxyliste->setPos(1700, 500);
    QObject::connect(liste, SIGNAL(clicked()), &w, SLOT(Export_Liste()));
    scene.addItem(proxyliste);

    QGraphicsProxyWidget* proxyplan = new QGraphicsProxyWidget();
    QPushButton* plan = new QPushButton("Export plan");
    plan->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    plan->setFixedSize(170, 40);
    proxyplan->setWidget(plan);
    proxyplan->setPos(1700, 550);
    QObject::connect(plan, SIGNAL(clicked()), &w, SLOT(Export_Plan()));
    scene.addItem(proxyplan);

    
   // QObject::connect(quitter, SIGNAL(clicked()), w, SLOT(quit()));
    /*QObject::connect(echanger, SIGNAL(clicked()), this, SLOT(echange()));
    QObject::connect(valider, SIGNAL(clicked()), this, SLOT(ouvrirDialogue()));*/
    

    

    

    
    for (int i = 0; i < 50; i++) {

        QGraphicsProxyWidget* proxy = new QGraphicsProxyWidget();

        switch (s->get_tables_salle()[i]->get_eleve_table()->get_numero_groupe())

        {
        case 1:
           
            proxy->setWidget(w.creation_bouton(s->id_table_index(i), 1));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        case 2:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i), 2));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        case 3:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i), 3));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        default:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i), 0));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        }

        scene.addItem(proxy);
        
    }

    for (int i = 50; i <= 79; i++) {

        QGraphicsProxyWidget* proxy = new QGraphicsProxyWidget();



        switch (s->get_tables_salle()[i]->get_eleve_table()->get_numero_groupe())

        {
        case 1:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i),1));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        case 2:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i),2));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        case 3:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i),3));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        default:
            proxy->setWidget(w.creation_bouton(s->id_table_index(i),0));
            proxy->setPos(s->get_tables_salle()[i]->get_x_table(), s->get_tables_salle()[i]->get_y_table());
            break;
        }
        proxy->setRotation(60);
        scene.addItem(proxy);
    }
}


int main(int argc, char* argv[])
{


    //Obligatoire
    QApplication a(argc, argv);
    MainWindow w;
    QGraphicsProxyWidget* proxy = new QGraphicsProxyWidget();
    QGraphicsScene scenebutton;
 
   

    /*QFileDialog dialog();
    dialog.setFileMode(QFileDialog::AnyFile);*/
    //Ecran d'accueil

    QLabel* label = new QLabel();
    QPixmap* pixmap_img = new QPixmap("logo.png");
    // mon_logo se trouve dans le repertoire qui contient mon exe
    label->setPixmap(*pixmap_img);
    QGraphicsProxyWidget* proxy2 = new QGraphicsProxyWidget();
    QGraphicsProxyWidget* proxy4 = new QGraphicsProxyWidget();
    proxy2->setWidget(label);
    QPushButton* Start = new QPushButton("Start");
    Start->setStyleSheet("QPushButton { background-color: #4E5BCD }");
    Start->setFixedSize(200, 50);
    scenebutton.addItem(proxy2);
    proxy4->setWidget(Start);
    proxy4->setPos(960, 540);
    QObject::connect(Start, SIGNAL(clicked()), &w, SLOT(ouverture_promo()));
    scenebutton.addItem(proxy4);
    scenebutton.setBackgroundBrush(Qt::yellow);



    //Ecran principal
   /* proxy->setWidget(w.affichage_promotion(w.get_promotion()));
    affichage_salle(w, w.get_salle(), w.get_promotion(), scenebutton);
    scenebutton.addItem(proxy);*/


    //Obligatoire
    w.setMouseTracking(true);
   
    QGraphicsView view(&scenebutton);
    view.setFixedSize(1920, 1080);
    view.showFullScreen();
    w.show();
   // w.affiche_vector();

    return a.exec();



}
