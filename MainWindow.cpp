#include "MainWindow.h"
#include "QtWidgets/qsizepolicy.h"
#include <QList>
#include <QtGui>
#include <QtCore>
#include <qlineedit.h>
#include <qmouseeventtransition.h>
#include <qcolor.h>
#include <qlabel.h>
#include <qvector.h>
#include <iostream>
#include <qgraphicsscene.h>
#include <QTableWidget>
#include <QTextCodec>
#include <qstring.h>
#include <fstream>
#include <QtWidgets/qfiledialog.h>
#include "main.cpp"


using namespace std;


MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent),
    m_pTableWidget(NULL)
{
    s = new Salle("Salle2A");
    p = new Promotion("2A", "C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\Fichiers_Claco.csv");
    //p->trie_nom();
    p->melange_promotion();
    s->creation_salle_ds_1();
    s->placement_aleatoire(p);
    ui.setupUi(this);
    initialisation_liste();
    
   
}
void MainWindow::initialisation_liste() {
    m_pTableWidget = new QTableWidget();
    m_pTableWidget->setRowCount(p->taille_promotion());
    m_pTableWidget->setColumnCount(4);
    m_TableHeader << "Nom" << "Prenom" << "Groupe" << "Table";
    m_pTableWidget->setHorizontalHeaderLabels(m_TableHeader);
    m_pTableWidget->verticalHeader()->setVisible(false);
    m_pTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_pTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_pTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    m_pTableWidget->setShowGrid(false);
    m_pTableWidget->setStyleSheet("QTableView {selection-background-color: red;}");
    m_pTableWidget->setGeometry(0, 0, 275, 1079);
    m_pTableWidget->verticalHeader()->setDefaultSectionSize(0);
    m_pTableWidget->setColumnWidth(0, 80);
    m_pTableWidget->setColumnWidth(1, 80);
    m_pTableWidget->setColumnWidth(2, 50);
    m_pTableWidget->setColumnWidth(3, 20);
    m_pTableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

}
void MainWindow::uncheck_all() {
    for (auto a : liste_bouton_table) {
        a->setChecked(false);
    }
    for (auto table : s->get_tables_salle()) {
        table->set_check(false);

    }
}
void MainWindow::echange() {
    int table_echangee_1;
    int couleur1;
    int table_echangee_2;
    int couleur2;
    bool done = 0;
    int cpt_table_echange = 0;
    for (auto a : s->get_tables_salle()) {
        if (a->get_check()) {

            cpt_table_echange++;
           // cout << cpt_table_echange;

        }
    }
    switch (cpt_table_echange)
    {
    case 0:
        QMessageBox::warning(this, "Avertissement", "Veuillez selectionner deux tables en cliquant dessus afin de pouvoir les echanger.");
        break;
    case 1:
        QMessageBox::warning(this, "Avertissement", "Veuillez selectionner une autre table en cliquant dessus afin de pouvoir les echanger.");
        break;
    case 2:
        while (done==0) {
            for (auto a : s->get_tables_salle()) {
                if (a->get_check() == 1) {
                    for (auto b : s->get_tables_salle()) {

                        if (b->get_numero_table() != a->get_numero_table() && b->get_check() == 1 && done==0) {
                            cout << endl;
                          //  cout << b->get_numero_table() + " " + b->get_eleve_table()->get_nom() << endl;
                           // cout << a->get_numero_table() + " " + a->get_eleve_table()->get_nom() << endl;
                            s->echange_deux_eleves(a, b);
                            table_echangee_1 = a->get_numero_table();
                            couleur1 = a->get_eleve_table()->get_numero_groupe();
                            table_echangee_2 = b->get_numero_table();
                            couleur2 = b->get_eleve_table()->get_numero_groupe();
                            //cout << b->get_numero_table() + " " + b->get_eleve_table()->get_nom() << endl;
                           // cout << a->get_numero_table() + " " + a->get_eleve_table()->get_nom() << endl;
                            done = true;
                        }
                    }
                }
            }
            int numero_bouton=0;
            for (auto a : liste_bouton_table) {
                if (a->text().toInt() == table_echangee_1) {
                    
                    switch (couleur1)
                    {
                    case 1:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #FF7C7A}QPushButton { background-color: #FC3431;border: 2px solid #000000 }");

                        break;
                    case 2:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #80B6B8}QPushButton { background-color: #24BABD ;border: 2px solid #000000 }");
                        break;
                    case 3:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #66CA81}QPushButton { background-color: #07BE38 ;border: 2px solid #000000 }");
                        break;

                    }
                }
                if (a->text().toInt() == table_echangee_2) {
                    switch (couleur2)
                    {
                    case 1:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #FF7C7A}QPushButton { background-color: #FC3431;border: 2px solid #000000 }");

                        break;
                    case 2:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #80B6B8}QPushButton { background-color: #24BABD ;border: 2px solid #000000 }");
                        break;
                    case 3:
                        a->setStyleSheet("QPushButton:checked{border: 30px solid #66CA81}QPushButton { background-color: #07BE38 ;border: 2px solid #000000 }");
                        break;
                    }
                }
                numero_bouton++;
            }
            uncheck_all();
        }
        break;
    default:
        QMessageBox::warning(this, "Avertissement", "Veuillez selectionner SEULEUMENT deux tables en cliquant dessus afin de pouvoir les echanger.");
        break;
    }
        
    affichage_promotion(p);
}

void MainWindow::afficherAxes()
{
    QPainter myPainter(this);
    myPainter.drawRect(50, 50, 1000, 600);
}

QPushButton* MainWindow::creation_bouton(QString name,int couleur)
{
        
    QPushButton* bouton1= new QPushButton(name);
   // cout<< bouton1->text().toStdString();
    bouton1->setFixedSize(30, 30);
    bouton1->setCheckable(true);
    switch (couleur)
    {
    case 1:
        bouton1->setStyleSheet ("QPushButton:checked{border: 30px solid #FF7C7A}QPushButton { background-color: #FC3431;border: 2px solid #000000 }");

        break;
    case 2:
        bouton1->setStyleSheet("QPushButton:checked{border: 30px solid #80B6B8}QPushButton { background-color: #24BABD ;border: 2px solid #000000 }");
        break;
    case 3:
        bouton1->setStyleSheet("QPushButton:checked{border: 30px solid #66CA81}QPushButton { background-color: #07BE38 ;border: 2px solid #000000 }");
        break;
    default:
        bouton1->setStyleSheet("QPushButton:checked{border: 30px solid #000000}QPushButton { background-color: #B5B8B6 ;border: 2px solid #000000 }");
        break;
    }
    liste_bouton_table.push_back(bouton1);
    QObject::connect(bouton1, SIGNAL(clicked()), this, SLOT(set_check()));
    //QObject::connect(bouton1, SIGNAL(hover()), this, SLOT(set_check()));

    return bouton1;
}
vector<QPushButton*> MainWindow::get_bouton_table() {
    return liste_bouton_table;
}
void MainWindow::affiche_vector() {
    for (auto i : liste_bouton_table) {
        cout << i->text().toStdString() << endl;
    }

}
vector<QPushButton*> MainWindow::table_checked() {
    vector<QPushButton*>table_check;
    for (auto a :liste_bouton_table) {
        if (a->isChecked()) {
            table_check.push_back(a);
            
        }
        
    }
    return table_check;
}
void MainWindow::ajout_table_supprimer() {
    int numero_bouton;
    for (auto table_check : liste_bouton_table) {
        if (table_check->text() == "X" && table_check->isChecked()) {
            table_check->setText(QString::number(numero_bouton+1));
            cout << table_check->text().toStdString();

        }
        numero_bouton = table_check->text().toInt();
    }
    uncheck_all();

}
void MainWindow::ouverture_promo()
{
    
   /* QString texte = QFileDialog::getOpenFileName(this, "Ouvrir");
    proxy->setWidget(w.affichage_promotion(w.get_promotion()));
    affichage_salle(w, w.get_salle(), w.get_promotion(), scenebutton);
    scenebutton.addItem(proxy);*/


}
void MainWindow::supprimerTable( ) {
    vector<QPushButton*>a = table_checked();
    int cpt = 0;
    for (auto table_check : a) {
        if (table_check->text() != "X") {
            cpt++;
        }
    }
    if (p->taille_promotion() > s->get_nb_table() - cpt) {
        QMessageBox::warning(this, "Avertissement", "Vous ne pouvez pas supprimer ces tables par manque de place");
        uncheck_all();
    }
    else {
        for (auto table_check : a) {
            if (table_check->text() != "X") {

                table_check->setText("X");
                table_check->setStyleSheet("{ background-color: #grey }");
            }
        }
    }
    uncheck_all();
}

void MainWindow::classement_place() {
    p->trie_place(s->classement_croissant());
    affichage_promotion(p);
}

void MainWindow::classement_nom()
{
    p->trie_nom();
    affichage_promotion(p);
}

void MainWindow::classement_prenom()
{
    p->trie_prenom();
    affichage_promotion(p);
}

void MainWindow::melanger_eleve() {
    p->trie_place(s->classement_croissant());
    affichage_promotion(p);

}
QCheckBox* MainWindow::creation_checkbox(QString name, int x, int y)
{
    QCheckBox* checkbox = new QCheckBox(name);
    checkbox->setGeometry(x, y, 400, 400);
    checkbox->setMinimumSize(200, 200);
    checkbox->setFont(QFont("Comic Sans MS", 12, 1, -1));

    checkbox->show();


    return checkbox;
}

void MainWindow::ouvrirDialogue(QString title, QString message) {


    QMessageBox::information(this, title, message);


}

QTableWidget* MainWindow::affichage_promotion(Promotion* p) {

    using namespace std;

      
try
{


    int i = 0;
    int cpt = 0;
    m_pTableWidget->clear();
    for (auto a : p->getclasse()) {
        cpt = 0;
        m_pTableWidget->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(p->get_eleve_promotion(i)->get_nom())));
        m_pTableWidget->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(p->get_eleve_promotion(i)->get_prenom())));
        m_pTableWidget->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(p->get_eleve_promotion(i)->get_groupe())));
        for (auto b : s->get_tables_salle()) {
            if (b->get_eleve_table()->get_nom() == p->get_eleve_promotion(i)->get_nom() && b->get_eleve_table()->get_prenom() == p->get_eleve_promotion(i)->get_prenom()) {
                m_pTableWidget->setItem(i, 3, new QTableWidgetItem(QString::number(b->get_numero_table())));

            }

            if (i == p->taille_promotion()) {
                m_pTableWidget->setItem(i, 3, new QTableWidgetItem("X"));
            }
            cpt++;
        }
        i++;
    }
}

catch (exception const& e)
{
    cerr << "ERREUR : " << e.what() << endl;
}

    
    return m_pTableWidget;
}
Salle* MainWindow::get_salle()
{
    return s;
}

Promotion* MainWindow::get_promotion()
{
    return p;
}   
void MainWindow::set_check()
{
    QPushButton* btn = qobject_cast<QPushButton*>(sender());
    QString table_numero = btn->text();
    cout << table_numero.toStdString();
    for (auto a : s->get_tables_salle()) {
        if ((a->get_numero_table()) == table_numero.toInt()) {

            a->modify_changed();
            // cout << "coucou" << a->get_check();

        }
    }
}
void MainWindow::ouvrirDialogue()
{
    QMessageBox::information(this, "wshe", "message");
}
void MainWindow::Export_CSS() {
    std::ofstream f("C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\template.css");
    if (f) {
        f << "table{margin-left : 10%;margin-top : 20px;display:inline-table;border-collapse: collapse;}";
        f << "td, th{border: 1px solid black;padding : 5px;}";
        f << "caption{caption-side: top;text-align:center;}";
        if (f) {
            f << ".plan {background-image:url(PlanVierge.png);background-repeat:no-repeat;}";

            for (int i = 1; i <= 7; i++) {
                if (liste_bouton_table[i-1]->text() == "X") {
                    f << ".span" << i << "{display : block;position: fixed;background: white;width: 80px;height: 50px;margin-left :" << 1803 - i * 73 << "px;margin-top:210px;}\n";
                }
            }
        
            for (int i = 8; i <= 15; i++) {
                if (liste_bouton_table[i]->text() == "X") {

                    f << ".span" << i << "{display : block;position: fixed;background: white;width: 80px;height: 50px;margin-left :" << 1307 - (i - 7) * 77 << "px;margin-top:210px;}\n";
                }
            }
                for (int i = 16; i <= 20; i++) {
                    if (liste_bouton_table[i]->text() == "X") {

                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 80px;height: 50px;margin-left :" << 1840 - (i - 15) * 85 << "px;margin-top:340px;}\n";
                    }
                }
                for (int i = 21; i <= 29; i++) {
                    if (liste_bouton_table[i]->text() == "X") {

                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 67px;height: 50px;margin-left :" << 1418 - (i - 20) * 68 << "px;margin-top:340px;}\n";
                    }
                }
                for (int i = 30; i <= 42; i++) {
                    if (liste_bouton_table[i]->text() == "X") {

                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 66px;height: 50px;margin-left :" << 1779 - (i - 29) * 69 << "px;margin-top:445px;}\n";
                    }
                }
                for (int i = 43; i <= 50; i++) {
                    if (liste_bouton_table[i]->text() == "X") {

                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 70px;height: 50px;margin-left :" << 1722 - (i - 42) * 70 << "px;margin-top:560px;}\n";
                    }
                }
                for (int i = 51; i <= 54; i++) {
                    if (liste_bouton_table[i]->text() == "X") {
                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 62px;height: 62px;margin-left :" << 275 - (i - 50) * 40 << "px;margin-top:" << 560 - (i - 50) * 55 << "px;transform: rotate(55deg);}\n";
                    }
                }
                for (int i = 55; i <= 60; i++) {
                    if (liste_bouton_table[i]->text() == "X") {
                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 62px;height: 62px;margin-left :" << 451 - (i - 54) * 41 << "px;margin-top:" << 596 - (i - 54) * 56 << "px;transform: rotate(55deg);}\n";
                    }
                }
                for (int i = 61; i <= 67; i++) {
                    if (liste_bouton_table[i]->text() == "X") {
                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 62px;height: 62px;margin-left :" << 561 - (i - 60) * 41 << "px;margin-top:" << 596 - (i - 60) * 56 << "px;transform: rotate(55deg);}\n";
                    }
                }
                for (int i = 68; i <= 74; i++) {
                    if (liste_bouton_table[i]->text() == "X") {
                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 62px;height: 62px;margin-left :" << 691 - (i - 67) * 41 << "px;margin-top:" << 576 - (i - 67) * 56 << "px;transform: rotate(55deg);}\n";
                    }
                }
                for (int i = 75; i <= 80; i++) {
                    if (liste_bouton_table[i-1]->text() == "X") {
                        f << ".span" << i << "{display : block;position: fixed;background: white;width: 62px;height: 62px;margin-left :" << 820 - (i - 74) * 41 << "px;margin-top:" << 575 - (i - 74) * 56 << "px;transform: rotate(55deg);}\n";
                    }
                }


        }
    }
}

void MainWindow::Export_Liste()
{
    Export_CSS();
    p->trie_nom();
    std::ofstream f("C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\liste_élève.html");
    if (f) {
        f << "<!DOCTYPE html>\n <html lang=\"fr\">\n <head>\n <meta charset=\"utf-8\">\n <title>Lyon1 PDC | Liste Élève </title>\n <link href=\"template.css\" rel=\"stylesheet\">\n <link rel=\"shortcut icon\" type=\"image/png\" \">\n </head>\n";

        f << "<table><caption>Groupe 1</caption><tr><th>Numero</th><th>Nom</th><th>Prenom</th><th>Table</th></tr>";
        f << "";
        for (int i = 0; i < p->taille_promotion(); i++) {
            if (p->get_eleve_promotion(i)->get_numero_groupe() == 1) {
                f << "<tr>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_numero_etu();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_nom();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_prenom();
                f << "</td>";
                f << "<td>";

                for (int j = 0; j < s->get_nb_table(); j++) {
                    if ((s->get_table_salle(j)->get_eleve_table() == p->get_eleve_promotion(i))) {
                        f << s->get_table_salle(j)->get_numero_table();
                    }
                }

                f << "</td>";
                f << "</tr>";
            }
        }
        f << "</table>";

        f << "<table><caption>Groupe 2</caption><tr><th>Numero</th><th>Nom</th><th>Prenom</th><th>Table</th></tr>";
        for (int i = 0; i < p->taille_promotion(); i++) {
            if (p->get_eleve_promotion(i)->get_numero_groupe() == 2) {
                f << "<tr>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_numero_etu();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_nom();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_prenom();
                f << "</td>";
                f << "<td>";

                for (int j = 0; j < s->get_nb_table(); j++) {
                    if ((s->get_table_salle(j)->get_eleve_table()->get_numero_etu() == p->get_eleve_promotion(i)->get_numero_etu()) && (s->get_table_salle(j)->get_eleve_table()->get_nom() == p->get_eleve_promotion(i)->get_nom()) && (s->get_table_salle(j)->get_eleve_table()->get_prenom() == p->get_eleve_promotion(i)->get_prenom())) {
                        f << s->get_table_salle(j)->get_numero_table();
                    }
                }

                f << "</td>";
                f << "</tr>";
            }

        }
        f << "</table>";

        f << "<table><caption>Groupe 3</caption><tr><th>Numero</th><th>Nom</th><th>Prenom</th><th>Table</th></tr>";
        for (int i = 0; i < p->taille_promotion(); i++) {
            if (p->get_eleve_promotion(i)->get_numero_groupe() == 3) {
                f << "<tr>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_numero_etu();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_nom();
                f << "</td>";
                f << "<td>";
                f << p->get_eleve_promotion(i)->get_prenom();
                f << "</td>";
                f << "<td>";

                for (int j = 0; j < s->get_nb_table(); j++) {
                    if ((s->get_table_salle(j)->get_eleve_table()->get_numero_etu() == p->get_eleve_promotion(i)->get_numero_etu()) && (s->get_table_salle(j)->get_eleve_table()->get_nom() == p->get_eleve_promotion(i)->get_nom()) && (s->get_table_salle(j)->get_eleve_table()->get_prenom() == p->get_eleve_promotion(i)->get_prenom())) {
                        f << s->get_table_salle(j)->get_numero_table();
                    }
                }

                f << "</td>";
                f << "</tr>";

            }

        }
        f << "</table>";
    }
    ShellExecute(NULL, TEXT("open"), TEXT("C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\liste_élève.html"), NULL, NULL, SW_SHOW);
}

void MainWindow::Export_Plan()
{
    Export_CSS();
    std::ofstream f("C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\plan.html");
    if (f) {
        f << "<!DOCTYPE html>\n <html lang=\"fr\">\n <head>\n <meta charset=\"utf-8\">\n <title>Lyon1 PDC | Plan </title>\n <link href=\"template.css\" rel=\"stylesheet\">\n <link rel=\"shortcut icon\" type=\"image/png\" \">\n </head>\n";
        f << "<body class = \"plan\">";
        for (int i = 1; i <= 80; i++) {
            f << "<span class = \"span" << i << "\"> </span>";
        }
        f << "</body>";
    }
    ShellExecute(NULL, TEXT("open"), TEXT("C:\\Users\\Perraud\\source\\repos\\Ptut - Copie\\plan.html"), NULL, NULL, SW_SHOW);
}
