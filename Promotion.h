#pragma once
// Cette classe va contenir tous les élèves et cela va permettre de pouvoir les récupérer et de les échanger de place...
#include"Eleve.h"
#include <vector>
#include <QString>

class Promotion
{
public:
	Promotion();
	Promotion(std::string name, std::string chemin);
	~Promotion();
	void trie_nom();
	void trie_prenom();
	void trie_place(std::vector<Eleve*> eleve_table);
	void add_eleve_classe(Eleve* ajouter);
	int taille_promotion();
	Eleve* get_eleve_promotion(int index);
	void melange_promotion();
	void lireexcel(std::string chemin);
	std::vector<Eleve*>getclasse();

private:
	std::string nom;
	std::vector<Eleve*>classe;
	std::string chemin;

};


