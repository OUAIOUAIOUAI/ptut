#include "Promotion.h"
#include <fstream>
#include <iostream>
#include <vector>
using namespace std;

Promotion::Promotion() :nom("")
{
}

Promotion::Promotion(string name, string file) : nom(name), chemin(file)
{
    lireexcel(file);

}

Promotion::~Promotion()
{

    classe.clear();
}
void Promotion::trie_nom() {
    vector<string>nom;
    string prenom_prece;
    bool verf;
    for (auto a : classe) {
        nom.push_back(a->get_nom());
    }
    sort(nom.begin(), nom.end());
    std::vector<Eleve*> classe2;
    for (auto a : nom) {
        verf = true;
        for (auto b : classe) {
            if ((b->get_nom() == a) && (b->get_prenom() != prenom_prece) && (verf == true)) {   //fix me pour plusieurs personne qui ont le meme nom
                prenom_prece = b->get_prenom();                                                 //rajouter une boucle pour faire la verif de tout les nom dans le if
                classe2.push_back(b);
                verf = false;
            }
        }
    }
    classe = classe2;

}
void Promotion::trie_prenom() {
    vector<string>prenom;
    string nom_prece;
    bool verf;
    for (auto a : classe) {
        prenom.push_back(a->get_prenom());
    }
    sort(prenom.begin(), prenom.end());
    std::vector<Eleve*> classe2;
    for (auto a : prenom) {
        verf = true;
        for (auto b : classe) {
            if ((b->get_prenom() == a) && (b->get_nom() != nom_prece) && (verf == true)) {   //fix me pour plusieurs personne qui ont le meme nom
                nom_prece = b->get_nom();                                                 //rajouter une boucle pour faire la verif de tout les nom dans le if
                classe2.push_back(b);
                verf = false;
            }
        }
    }
    classe = classe2;



}

void Promotion::trie_place(vector<Eleve*>eleve_table) {
    std::vector<Eleve*> classe2;
    for (auto eleve_non_place : classe) {
        if (eleve_non_place->get_place() == false) {
            cout << "caca";
            classe2.push_back(eleve_non_place);
            cout << eleve_non_place->get_nom();
        }
    }

 
    for (auto a : eleve_table) {
            classe2.push_back(a);    
        
    }
  
    classe = classe2;

}
void Promotion::add_eleve_classe(Eleve* ajouter)
{
    classe.push_back(ajouter);
}

int Promotion::taille_promotion()
{
    return classe.size();
}

Eleve* Promotion::get_eleve_promotion(int index)
{
    int compteur_eleve = 0;
    for (auto eleve : classe) {
        if (compteur_eleve == index) {
            return eleve;
        }
        compteur_eleve++;
    }
}
void Promotion::melange_promotion() {

    random_shuffle(classe.begin(), classe.end());
 
}




void Promotion::lireexcel(string chemin)
{

    ifstream fichier(chemin);
    if (fichier)
    {
        string ligne;
        string numero;
        string nom;
        string prenom;
        string groupe;
        string date;

        while (getline(fichier, numero, ';')) {

            getline(fichier, groupe, ';');
            getline(fichier, prenom, ';');
            getline(fichier, nom, ';');
            getline(fichier, date);



            //cout << numero << " " << groupe << " " << prenom << " " << nom << " " << date << endl;
            Eleve* nouvel_eleve = new Eleve(numero, nom, prenom, groupe, date);
            add_eleve_classe(nouvel_eleve);
            

        }

    }
   
    else
    {
        cout << " erreur" << endl;

    }
   /* for (auto a : classe) {
        cout << a->get_nom();
    }*/
}

std::vector<Eleve*> Promotion::getclasse()
{
    return classe;
}



                                                                        
