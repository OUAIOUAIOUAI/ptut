#pragma once
//Cette classe représente les tables de la salle de ds avec un numéro pour le plan, un pointeur sur l'élève qui va être assis et un attribut check pour savoir si elle est sélectionnée ( pour faire l'échange)
#include "Eleve.h"
class Table
{
private:
	int numero_table;
	Eleve* eleve_table;
	int ligne;
	int colonne;
	bool check; // si toute la salle n'est pas faite de la même façon

public:
	Table();
	Table(int id, Eleve* eleve_a_pointer, int x, int y, bool checked);
	~Table();
	int get_numero_table();
	bool get_check();
	void modify_changed();
	Eleve* get_eleve_table();
	int get_x_table();
	int get_y_table();
	void set_x_table(int co_x);
	void set_y_table(int co_y);
	void set_numero_table(int num_table);
	void set_eleve_table(Eleve* eleve_sur_table);
	void set_check(bool a);

};

