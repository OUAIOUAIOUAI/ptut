#pragma once
#include <iostream>
#include "Table.h"
#include <vector>
#include "Promotion.h"
#include <QString>
class Salle
{
public:
	Salle();
	Salle(std::string name);
	~Salle();
	int get_nb_table();
	void add_tables_salle(Table* table_to_add);
	void delete_tables_salle(int index);
	void echange_deux_eleves(Table* t1, Table* t2);
	void afficher_tables();
	void creation_salle(Promotion* p);
	void creation_salle_ds_1();
	void modify_changed(QString id_table);
	Table* get_table_salle(int index);
	bool placement_aleatoire(Promotion*& lapromo);
	QString id_table_index(int index);		// va retourner l'id d'une table avec un index
	std::vector<Table*> get_tables_salle();
	bool acote(Table* table1, Table* table2);
	std::vector<Eleve*> classement_croissant();
	bool placement_groupe(Promotion*& lapromo);
private:
	std::string nom_salle;
	std::vector<Table*>tables_salle;

};
