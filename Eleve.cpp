#include "Eleve.h"

Eleve::Eleve():numero_etu(""),nom(""),prenom(""),groupe(""),sous_groupe(""),place(false)
{
}
Eleve::Eleve(std::string num_etu, std::string last_name, std::string name, std::string group, std::string sub_group) : numero_etu(num_etu), nom(last_name), prenom(name), groupe(group), sous_groupe(sub_group), place(false)
{
}
Eleve::~Eleve()
{
}

std::string Eleve::get_nom()
{
	return nom;
}
int Eleve::get_numero_groupe()
{
    for (int i = 0; i < get_groupe().length(); i++) {
        if (get_groupe()[i] == '.') {
            return get_groupe()[i - 1] - 48;
        }
    }
    return 0;
}
std::string Eleve::get_numero_etu() {
    return numero_etu;
}

int Eleve::get_numero_ssgroupe()
{
    for (int i = 0; i < get_groupe().length(); i++) {
        if (get_groupe()[i] == '.') {
            return get_groupe()[i + 1] - 48;
        }
    }
    return 0;
}

std::string Eleve::get_prenom()
{
	return prenom;
}

std::string Eleve::get_groupe()
{
	return groupe;
}

std::string Eleve::get_sous_groupe()
{
	return sous_groupe;
}

bool Eleve::get_place()
{
	return place;
}

void Eleve::set_place(bool Place)
{
	place = Place;
}

QString Eleve::get_nom_eleve()
{
	return QString::fromStdString(nom);
}
