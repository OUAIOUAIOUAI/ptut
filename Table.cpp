#include "Table.h"

Table::Table() :numero_table(0), eleve_table(nullptr), colonne(0), ligne(0)
{

}

Table::Table(int id, Eleve* eleve_a_pointer, int x, int y, bool checked) : numero_table(id), eleve_table(eleve_a_pointer), ligne(x), colonne(y), check(checked)
{
}

Table::~Table()
{
    delete eleve_table;
}

void Table::set_check(bool a) {
    check = a;
}
int Table::get_numero_table()
{
    return numero_table;
}

bool Table::get_check()
{
    return check;
}

void Table::modify_changed()
{
    check = !check;
   
    
}

Eleve* Table::get_eleve_table()
{
    return eleve_table;
}

int Table::get_x_table()
{
    return ligne;
}

int Table::get_y_table()
{
    return colonne;
}

void Table::set_x_table(int co_x)
{
    ligne = co_x;
}

void Table::set_y_table(int co_y)
{
    colonne = co_y;
}

void Table::set_numero_table(int num_table)
{
    numero_table = num_table;
}

void Table::set_eleve_table(Eleve* eleve_sur_table)
{
    eleve_table = new Eleve();
    eleve_table = eleve_sur_table;
}
