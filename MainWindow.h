#pragma once

#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include "ui_MainWindow.h"
#include <QString>
#include <QWidget>
#include <QPainter>
#include <qlist.h>
#include <qmessagebox.h>
#include <qlist.h>
#include <qlistview.h>
#include "Salle.h"
#include "Promotion.h"
#include <QListWidget>
#include <qlistwidget.h>
#include <QCheckBox>
#include <QTableView>
#include <qsignalmapper.h>
#include <QStandardItemModel>
#include <QTableWidget>
#include <QHeaderView>
#include <vector>


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget* parent = Q_NULLPTR);

    void afficherAxes();
    QPushButton* creation_bouton(QString name, int couleur);
    std::vector<QPushButton*> get_bouton_table();
    void affiche_vector();
    std::vector<QPushButton*> table_checked();
    QCheckBox* creation_checkbox(QString name, int x, int y);
    QTableWidget* affichage_promotion(Promotion* p);
    Salle* get_salle();
    void Export_CSS();
    Promotion* get_promotion();
signals :
    void monSignal(QString);


public slots:
    void Export_Liste();
    void Export_Plan();
    void classement_place();
    void classement_nom();
    void classement_prenom();
    void melanger_eleve();
    void ouvrirDialogue(QString title, QString message);
    void ouvrirDialogue();
    void set_check();
    void initialisation_liste();
    void uncheck_all();
    void echange();
    void supprimerTable();
    void ajout_table_supprimer();
    void ouverture_promo();


private:

    Ui::MainWindowClass ui;
    Salle* s;
    Promotion* p;
    std::vector<QPushButton*>liste_bouton_table;
    QSignalMapper* signalMapper;
    QListWidget* promo = new QListWidget(this);
    QPushButton* quitter;
    QTableWidget* m_pTableWidget;
   /* QPushButton* echanger;
    QPushButton* melanger;
    QPushButton* ajouter_eleve;
    QPushButton* supprimer_table;
    QPushButton* retirer_eleve;
    QPushButton* supprimer_ligne;
    QPushButton* valider;
    QPushButton* ajouter_table;*/
    QStringList m_TableHeader;

};
