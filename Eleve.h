#pragma once
// cette classe va servir à représenter les élève que le professeur va fournir pour leur donner un id et une place
#include <string>
#include <QString>

class Eleve
{
private :
	std::string numero_etu;
	std::string nom;
	std::string prenom;
	std::string groupe;
	std::string sous_groupe;
	bool place;
	
public :
	Eleve();
	Eleve(std::string num_etu, std::string last_name, std::string name, std::string group, std::string sub_group);
	~Eleve();
	std::string get_nom();
	int get_numero_groupe();
	std::string get_numero_etu();
	int get_numero_ssgroupe();
	std::string get_prenom();
	std::string get_groupe();
	std::string get_sous_groupe();
	bool get_place();
	void set_place(bool Place);
	QString get_nom_eleve();

};

