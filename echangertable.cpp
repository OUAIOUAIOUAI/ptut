#include "eleve.h"
#include "table.h"
#include <iostream>
#include "Promotion.h"
#include "Salle.h"
using namespace std;


void echange_deux_eleves(Table* t1, Table* t2) {

	Table* echange = new Table();

	if(t1->get_eleve_table() != nullptr && t2->get_eleve_table() != nullptr) {   // test pour savoir s'il y a bien des élèves sur les deux tables.

		echange->set_eleve_table(t1->get_eleve_table());


		t1->set_eleve_table(t2->get_eleve_table());  // on fait l'échange de place en 3 temps avec une table temporaire


		t2->set_eleve_table(echange->get_eleve_table());
	}

	if (t1->get_eleve_table() == nullptr && t2->get_eleve_table()!=nullptr){  // la table 1 est vide donc on déplace juste l'élève
		
		t1->set_eleve_table(t2->get_eleve_table());

		t2->set_eleve_table(nullptr);
	}

	if (t2->get_eleve_table() == nullptr && t1->get_eleve_table() != nullptr) { // la table 2 est vide donc on déplace juste l'élève

		t2->set_eleve_table(t1->get_eleve_table());

		t1->set_eleve_table(nullptr);
	}

}
void creation_salle(Promotion* p) {
	Salle* salle_ds = new Salle("Salle de DS");
	for (int x = 0;x < p->taille_promotion();x++){

		Table* table_to_add = new Table(x,nullptr,x,x);
		salle_ds->add_tables_salle(table_to_add);
		
	}


}

void supprimer_table(Table* table_a_supprimer,Salle* salle_ds){
	
	salle_ds->add_tables_salle(table_a_supprimer);


}







int main() {
	Promotion* alooo = new Promotion("2A","effe");
	creation_salle(alooo);

}
