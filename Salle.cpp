#include "Salle.h"
#include "time.h"
#include <vector>
#define square(a)  (a)*(a)
using namespace std;

Salle::Salle() :nom_salle("")
{
}


Salle::Salle(string nom) : nom_salle(nom)
{
}

Salle::~Salle()
{
	tables_salle.clear();
}


int Salle::get_nb_table()
{
	return tables_salle.size();
}

void Salle::add_tables_salle(Table* table_to_add)
{
	tables_salle.push_back(table_to_add);

}

void Salle::delete_tables_salle(int index)
{

	tables_salle.erase(tables_salle.begin() + index);
}

void Salle::echange_deux_eleves(Table* t1, Table* t2) {

	Table* echange = new Table();
	

	if (t1->get_eleve_table() != nullptr && t2->get_eleve_table() != nullptr) {   // test pour savoir s'il y a bien des lves sur les deux tables.

		echange->set_eleve_table(t1->get_eleve_table());


		t1->set_eleve_table(t2->get_eleve_table());  // on fait l'change de place en 3 temps avec une table temporaire


		t2->set_eleve_table(echange->get_eleve_table());

	}

	if (t1->get_eleve_table() == nullptr && t2->get_eleve_table() != nullptr) {  // la table 1 est vide donc on dplace juste l'lve

		t1->set_eleve_table(t2->get_eleve_table());

		t2->set_eleve_table(nullptr);
	}

	if (t2->get_eleve_table() == nullptr && t1->get_eleve_table() != nullptr) { // la table 2 est vide donc on dplace juste l'lve

		t2->set_eleve_table(t1->get_eleve_table());

		t1->set_eleve_table(nullptr);
	}
	
}
void Salle::afficher_tables()
{
	for (int i = 0; i < tables_salle.size(); i++) {

		cout << "table " << tables_salle[i]->get_numero_table() << endl;
		cout << "Eleve a la table :";
		if (tables_salle[i]->get_eleve_table() == nullptr) {
			cout << " Aucun" << endl;
			cout << "coordonnee" << tables_salle[i]->get_x_table() << ";" << tables_salle[i]->get_y_table() << endl;
		}
		else {
			cout << tables_salle[i]->get_eleve_table() << endl;

		}
	}

}

void Salle::creation_salle(Promotion* p) {

	/*Table* table_to_add1 = new Table(1, nullptr, 1, 1);
	Table* table_to_add2 = new Table(2, nullptr, 1, 2);
	Table* table_to_add3 = new Table(3, nullptr, 1, 3);
	tables_salle.push_back(table_to_add1);
	tables_salle.push_back(table_to_add2);
	tables_salle.push_back(table_to_add3);*/
	//for (int x = 0; x < p->taille_promotion(); x++) {

	//	Table* table_to_add = new Table(x, nullptr, x, x);
	//	salle_ds->add_tables_salle(table_to_add);

	//}
}

void Salle::creation_salle_ds_1()
{

	int x = 1800; // coordonnee de la ligne de la table
	for (int i = 1; i <= 15; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, 100, 0));
		x = x - 60;
	}
	x = 1810;
	for (int i = 16; i <= 29; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, 160, 0));
		x = x - 60;
	}
	x = 1800;
	for (int i = 30; i <= 42; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, 220, 0));
		x = x - 60;

	}
	x = 1750;
	for (int i = 43; i <= 50; i++) {

		tables_salle.push_back(new Table(i, nullptr, x, 280, 0));
		x = x - 60;
	}
	x = 560;
	int y = 180;

	for (int i = 51; i <= 54; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, y, 0));
		x = x + 25;
		y = y + 40;
	}
	x = 610;
	y = 140;
	for (int i = 55; i <= 60; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, y, 0));
		x = x + 25;
		y = y + 40;
	}
	x = 660;
	y = 100;
	for (int i = 61; i <= 67; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, y, 0));
		x = x + 25;
		y = y + 40;
	}
	x = 740;
	y = 100;
	for (int i = 68; i <= 74; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, y, 0));
		x = x + 25;
		y = y + 40;
	}
	x = 820;
	y = 100;
	for (int i = 75; i <= 80; i++) {
		tables_salle.push_back(new Table(i, nullptr, x, y, 0));
		x = x + 25;
		y = y + 40;
	}



}

void Salle::modify_changed(QString id_table)
{
	int id = id_table.toInt();
	for (auto a : tables_salle) {
		if (a->get_numero_table() == id) {
			a->modify_changed();
		}
	}
}
Table* Salle::get_table_salle(int index)
{
	int compteur_table = 0;
	for (auto table : tables_salle) {
		if (compteur_table == index) {
			return table;
		}
		compteur_table++;
	}
}
bool Salle::placement_aleatoire(Promotion*& lapromo)
{
	int random;
	srand(time(NULL));
	for (int i = 0; i < lapromo->getclasse().size(); i++) {

		random = rand() % (lapromo->getclasse().size());

		while (lapromo->getclasse()[random]->get_place() == true) {
			random = rand() % (lapromo->getclasse().size());
		}
		tables_salle[i]->set_eleve_table(lapromo->getclasse()[random]);
		lapromo->getclasse()[random]->set_place(true);
		//cout << i + 1 << " " << tables_salle[i]->get_eleve_table()->get_place() << " " << tables_salle[i]->get_eleve_table()->get_nom() << endl;
	}
	return false;
}


QString Salle::id_table_index(int index)
{
	QString id_table = QString::number(tables_salle[index]->get_numero_table());
	return  id_table;


}

vector<Table*> Salle::get_tables_salle()
{
	return tables_salle;
}

bool Salle::acote(Table* table1, Table* table2)
{
	if (table1 != table2) {
		float dist = sqrtf(square(table1->get_x_table() - table2->get_x_table()) + square(table1->get_y_table() - table2->get_y_table()));
		if (table1->get_numero_table() > 50 && table2->get_numero_table() > 50) {
			if ((table1->get_x_table() == table2->get_x_table() + 25 && table1->get_y_table() == table2->get_y_table() + 40) || (table1->get_x_table() == table2->get_x_table() - 25 && table1->get_y_table() == table2->get_y_table() - 40)
				|| (table1->get_x_table() == table2->get_x_table() + 50 && table1->get_y_table() == table2->get_y_table() - 40) || (table1->get_x_table() == table2->get_x_table() - 50 && table1->get_y_table() == table2->get_y_table() + 40)) {
				return true;
			}
		}
		if ((table1->get_numero_table() <= 50 && table2->get_numero_table() <= 50) && dist < 70) {
			
			return true;
		}
	}
	return false;
}
vector<Eleve*> Salle::classement_croissant() {
	vector<Eleve*>eleve_table;
	for (auto a : tables_salle) {
		eleve_table.push_back(a->get_eleve_table());
	}
	return eleve_table;

}
bool Salle::placement_groupe(Promotion*& lapromo)

{
	bool ouye = true;
	vector<string>groupeAEviter;
	for (int i = 0; i < tables_salle.size(); i++) {
		groupeAEviter.clear();
		for (int j = 0; j <= i; j++) {
			if ((acote(tables_salle[i], tables_salle[j]) == true) && (tables_salle[j]->get_eleve_table() != nullptr)) {
				cout << tables_salle[i]->get_numero_table() << " " << tables_salle[j]->get_numero_table() << endl;
				groupeAEviter.push_back(tables_salle[j]->get_eleve_table()->get_groupe());
			}
		}
		for (auto a : lapromo->getclasse()) {
			ouye = true;
			for (int z = 0; z < groupeAEviter.size(); z++) {
				//cout << groupeAEviter[z] << endl;
				if (a->get_place() == true || a->get_groupe() == groupeAEviter[z]) {
					ouye = false;
				}

			}
			if (ouye == true) {
				tables_salle[i]->set_eleve_table(a);
				a->set_place(true);
				break;
			}

		}
		//cout << "blllllllll;";

	}
	return false;
}
